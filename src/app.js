import 'normalize.css';
import './app.css';

const botonDado = document.querySelector('.tirar_dado');
const botonDarkMode = document.querySelectorAll('.dark_mode_btn');
botonDarkMode.forEach(boton => {
    boton.addEventListener('click', (e) => {
        console.log('click');
        e.preventDefault();
        document.querySelector('body').classList.toggle('dark');
    });
});

let dadoEnMovimiento = false;

botonDado.addEventListener('click', (e) => {
    e.preventDefault();
    if (dadoEnMovimiento) {
        return;
    }
    dadoEnMovimiento = true;
    botonDado.classList.add('disabled');
    let actual = 1;
    // 4 máximo de segundos, 1 el mínimo
    let segundos = Math.floor((Math.random() * (4 - 1) + 1) * 1000);
    let cubo = setInterval(() => {
        document.querySelector(`[data-numero="${actual}"]`).classList.add('ocultar');
        if (actual === 6) {
            actual = 1;
        } else {
            actual++;
        }
        document.querySelector(`[data-numero="${actual}"]`).classList.remove('ocultar');
    }, 50);
    setTimeout(() => {
        clearInterval(cubo);
        dadoEnMovimiento = false;
        botonDado.classList.remove('disabled');
    }, segundos);
});