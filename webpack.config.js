const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/app.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/app.js'
    },
    // Creamos el configurador de módulos
    module: {
        // Las reglas son un array de objetos Rule
        rules: [
            {
                // Todos los ficheros .js
                test: /\.js$/,
                // Excluimos node_modules pues
                // no son ficheros nuestros
                exclude: '/node_modules/',
                // Qué usar
                use: {
                    // Indicamos que Babel-loader
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(html)$/,
                use: {
                  loader: 'html-loader',
                },
            },
        ]
    },
    devServer: {
        contentBase: './dist',
        host: '127.0.0.1'
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        })
    ]
}